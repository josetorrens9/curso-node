const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const mongoose = require('mongoose')
const validate = require('express-validation')
const routes = require('./server/routes')
const app = express()

app.set('port', process.env.PORT || 3000)
app.set('view engine', 'pug')
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(cookieParser())

app.use(routes)

app.use((err, req, res, next) => {
  if (err instanceof validate.ValidationError) {
    const error = new Error(err.errors, err.status, true);
    res.status(err.status).json(err.errors)
  }
    return next(err);
});

app.listen(app.get('port'), (err) => {
  if(err) return console.log('Crash'), process.exit(1)
  console.log(`Sucre adventure is running in port ${app.get('port')}`)
})

const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let activitySchema = new Schema({
    name:{ type: String, unique: true },
    images: {
        photo:{ type: String },
        thumbnail: { type: String },
    },
    description: { type: String }
})

module.exports = mongoose.model('activity', activitySchema);
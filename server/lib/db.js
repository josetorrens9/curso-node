'use strict'
const config = require('../config.json').db
const mongoose = require('mongoose')
require('../model')
const activityModel = mongoose.model('activity')

class DataBase {

    constructor(info){
        this.name = info.name || config.name
        this.user = info.user || config.user
        this.port = info.port || config.port
        this.host = info.host || config.host
    }

    connection(){
        const url = `${this.user}://${this.host}:${this.port}/${this.name}`
        mongoose.connect(url)
        mongoose.connection.on('error', ()=> {
            throw new Error(`No se puede conectar a la base de datos ${this.name}`)
        })
    }

	create(obj, callback) {
		const model = new activityModel(obj)
		model.save(model, (err, created)=> {
			if(err) callback(err)

			callback(null, created)
		})
	}
    findActivities(query, limit, callback){
        activityModel.find(query, null, {limit:limit}, (err, data)=> {
			if(err) callback(err)

			callback(null, data)            
        })
    }

    findActivity(query, callback){
        activityModel.findOne(query, (err, activity)=> {
			if(err) callback(err)

			callback(null, activity)            
        })        
    }

    delete(id, callback){
        activityModel.findByIdAndRemove({ _id:id }, (err, data)=> {
			if(err) callback(err)

			callback(null, data)             
        })
    }

    edit(obj, callback){
		activityModel.update({ _id: obj._id}, obj, (err, activity)=> {
			if(err) callback(err)

			callback(null, activity)
		})
    }
}

module.exports = DataBase;
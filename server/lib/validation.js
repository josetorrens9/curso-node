const Joi = require('joi')

module.exports = {
  createActivity: {
    body: {
      name: Joi.string().required(),
      photo: Joi.string().required(),
      thumbnail: Joi.string().required(),
      description: Joi.string().required(),
    }
  }
}
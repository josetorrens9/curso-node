const express = require('express'),
    router = express.Router(),
    validate = require('express-validation')
    paramsValidate = require('../lib/validation')
    db = require('../lib/db')

const DB = new db({})
DB.connection()

router.get('/', (req, res, next) =>{
    res.render('index')
})   

router.get('/api/activity', (req, res, next) =>{
    /*Trae un listado de actividades */
    DB.findActivities({}, 0, (err, list)=> {
        if(err) res.status(500).json(err)

        res.status(200).json(list)        
    })
})

router.get('/api/activity/others/:id', (req, res, next) =>{
    /*Trae un listado de actividades relacionado al ID */
    let limit = 3
    DB.findActivities({ _id: { $not:{ $eq:req.params.id } } }, limit, (err, list)=> {
        if(err) res.status(500).json(err)

        res.status(200).json(list)           
    })
})

router.post('/api/activity', validate(paramsValidate.createActivity), (req, res, next) =>{
    /*Crear una actividad */
    let activityBody = {
        name: req.body.name,
        images: {
            photo: req.body.photo,
            thumbnail: req.body.thumbnail
        },
        description: req.body.description
    }

    DB.create(activityBody, (err, created)=> {
        if(err) res.status(500).json(err)

        res.status(201).json(created)
    })
})

router.get('/api/activity/:id', (req, res, next) =>{
    /*Obtener una actividad */
    DB.findActivity({_id:req.params.id}, (err, activity)=> {
        if(err) res.status(500).json(err)

        res.status(200).json(activity)
    })
})

router.delete('/api/activity/:id', (req, res, next) =>{
    /*Borrar una actividad */
    DB.delete(req.params.id, (err, data)=> {
        if(err) res.status(500).json(err)

        if(data)
            res.status(204)
        else
            res.status(404)
    })
})

router.put('/api/activity/:id', validate(paramsValidate.createActivity), (req, res, next) =>{
    /*Modificar una actividad */
        let activityBody = {
        _id:req.params.id,
        name: req.body.name,
        images: {
            photo: req.body.photo,
            thumbnail: req.body.thumbnail
        },
        description: req.body.description
    }
    
    DB.edit(activityBody, (err, activity)=> {
        if(err) res.status(500).json(err)

        res.status(200).json(activity)        
    })
})

router.get('*', (req, res, next) =>{
    res.render('index')
})   

module.exports = router;
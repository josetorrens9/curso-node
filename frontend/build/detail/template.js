const yo = require('yo-yo')
const modal = require('../components/modal')
const secondList = require('../components/second-list')

module.exports = function(item){
 	return yo`<main onload=${init.bind()}>
                <div class="parallax-container">
                  <div class="parallax"><img src="${item.images.photo}" alt=""></div>
                </div>   
                <div class="row">
                <div class="col s12 section-title">
                    ${item.name}
                </div>  
                </div>      
                <div class="container">
                <div class="row">
                    <div class="item">
                    <div class="title">Descripcion</div>
                    <p>${item.description}</p>
                    </div>
                    <div class="item">
                    <div class='share-icons'>
                        <button class='facebook'><i class="fa fa-facebook"></i></button>
                        <button class='google'><i class="fa fa-google-plus"></i></button>
                        <button class='twitter'><i class="fa fa-twitter"></i></button>
                    </div>
                    </div>            
                    <div class="item">
                    <div class="title">Otras actividades</div>
                        ${secondList(item._id)}
                    </div>                                                                                                             
                </div>
                </div>
                ${modal()}
            </main>`;	
}

function init() {
    $('.parallax').parallax();
    
    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
        }
    );       

    $('.modal').modal()
}

const template = require('./template')
const loading = require('../components/loading')
const api = require('../../store/api')

module.exports = function (id) {

    $('#app-container').append(loading)
    
        setTimeout(function() {

            api.getActivity(id, (err, activity)=> {
                $('main').remove();
                $('#app-container').append(template(activity))
            })

        }, 3000);

}
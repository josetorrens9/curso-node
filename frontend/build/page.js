const page = require('page');
const empty = require('empty-element');
const header = require('./header')
const home = require('./home')
const detail = require('./detail')


page('/', (ctx, next) => {
    let main = document.getElementById('app-container');
    empty(main).appendChild(header())
    home()
})

page('/detail/:id', (ctx, next) => {
    let main = document.getElementById('app-container');
    empty(main).appendChild(header())
    detail(ctx.params.id)
})
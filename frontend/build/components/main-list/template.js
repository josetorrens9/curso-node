const yo = require('yo-yo')
const modalTemplate = require('../modal')
const api = require('../../../store/api')

module.exports = function(loading, list){
    if(loading){
        return yo`<div class="row loading-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                        </div>
                    </div>
                  </div>`
    }

        return yo`<div id='main-list' class="row" onload=${init.bind()}>
                  ${list.map((item, index)=>{
                      return yo`<div class="col s12 m6 l4">
                                  <div class="card">
                                    <!-- Dropdown Trigger -->
                                    <a class='dropdown-button-more more-list' href='#' data-activates='dropdown-card-${index}'><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>

                                    <!-- Dropdown Structure -->
                                    <ul id='dropdown-card-${index}' class='dropdown-content'>
                                        <li onclick=${deleteActivity.bind(null, item)}><a>Eliminar Actividad</a></li>
                                        <li onclick=${modalEdit.bind(null, item)}><a>Editar Actividad</a></li>
                                    </ul>                                  
                                    <div class="card-image">
                                        <img src="${item.images.thumbnail}">
                                        <a href='/detail/${item._id}'><span class="card-title">${item.name}</span></a>
                                    </div>
                                    <div class="card-content">
                                        <p class='truncate'>${item.description}</p>
                                    </div>
                                  </div>
                              </div>`;
                      })}
              </div>`;	

}

function init() {
  
  $('.dropdown-button-more').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    }
  );  
}

function modalEdit(item){
    console.log(item)
    let modal =  $('#modal')[0]

    yo.update(modal, modalTemplate(item))
    
    $('#modal').modal('open')
}

function deleteActivity(item){
    api.deleteActivity(item._id, (err, data)=>{
        if(err) api.showToast('Ha ocurrido un error')
        else api.showToast(`La actividad ${item.name} ha sido eliminada`)        
    })
}
const yo = require('yo-yo')
const api = require('../../store/api')

module.exports = function(data){
    let content = data || {name:'', images:{thumbnail:'', photo:''}, description:''}
    return yo `<div id="modal" class="modal" onload=${init.bind()}>
                    <div class="modal-content">
                    <h4>Crear Actividad</h4>
                    <form>
                        <fieldset>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="activity_name" type="text" value='${content.name}'>
                                <label class="${content._id ? 'active':''}" for="activity_name">Nombre actividad</label>
                            </div>                
                            <div class="input-field col s12">
                                <input id="thumb" type="text" value='${content.images.thumbnail}'>
                                <label class="${content._id ? 'active':''}" for="thumb">Url thumbnail</label>
                            </div>                
                            <div class="input-field col s12">
                                <input id="photo" type="text" value='${content.images.photo}'>
                                <label class="${content._id ? 'active':''}" for="photo">Url imagen principal</label>
                            </div>                                                
                            <div class="input-field col s12">
                                <textarea id="description" class="materialize-textarea">${content.description}</textarea>
                                <label class="${content._id ? 'active':''}" for="description">Descripcion</label>
                            </div>                
                        </div>
                        </fieldset>
                    </form>
                    </div>
                    <div class="modal-footer">
                    <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
                    <a onclick=${saveOrEdit.bind(null, content)} class="waves-effect waves-green btn-flat">Guardar</a>
                    </div>
                </div>`;
}

function init() {
  $('#modal').modal({
      complete: function() { 
        $('#modal form input').val('')
        $('#modal form textarea').val('')
      }
    }
  );    
}

function saveOrEdit(content){
    let activity = {
        name: $('#activity_name').val(),
        photo: $('#photo').val(),
        thumbnail: $('#thumb').val(),
        description: $('#description').val()
    }

    if(content._id)
        activity._id = content._id

        console.log(activity)
        
    api.saveOrEditActivity(activity, (err, data) => {
        if(err) api.showToast('Ha ocurrido un error')
        else{
            api.showToast(`La actividad ${activity.name} ha sido guardada`)
            $('#modal').modal('close');
        }
    }) 
}
const yo = require('yo-yo')

module.exports = function(loading, list){
    if(loading){
        return yo`<div class="row loading-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                        </div>
                    </div>
                  </div>`
    }
    return yo`<div class="row">
                  ${list.map((item)=>{
                      return yo`<div class="col s12 m6 l4">
                                  <div class="card">
                                  <div class="card-image">
                                      <img src="${item.images.thumbnail}">
                                      <a href='/activity/${item._id}'><span class="card-title">${item.name}</span></a>
                                  </div>
                                  <div class="card-content">
                                      <p class='truncate'>${item.description}</p>
                                  </div>
                                  </div>
                              </div>`;
                      })}
              </div>`;	
}
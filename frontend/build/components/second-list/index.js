const yo = require('yo-yo')
const template = require('./template')
const api = require('../../../store/api')

module.exports = function(id) {
    
    var content = template(true) 
    setTimeout(function() {
        
        api.getOthers(id, (err, list)=> {
            yo.update(content, template(false, list))
        })

    }, 3000);
    
    return content;
}
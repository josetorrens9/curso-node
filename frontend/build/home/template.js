const yo = require('yo-yo')
const list = require('../components/main-list')
const modal = require('../components/modal')
module.exports = function(){
 	return yo`<main onload=${init.bind()}>
                <div class="slider">
                    <ul class="slides">
                        <li>
                        <img src="/img/slides/1.jpg"> <!-- random image -->
                        <div class="caption center-align">
                            <h3>Visita mochima con nosotros!</h3>
                        </div>
                        </li>
                        <li>
                        <img src="/img/slides/2.jpg"> <!-- random image -->
                        <div class="caption left-align">
                            <h3>Sube al Roraima</h3>
                            <h5 class="light grey-text text-lighten-3">Vive la experiencia!!</h5>
                        </div>
                        </li>
                        <li>
                        <img src="/img/slides/3.jpg"> <!-- random image -->
                        <div class="caption right-align">
                            <h3>Bucea en mochima</h3>
                            <h5 class="light grey-text text-lighten-3">y conoce el mundo acuatico</h5>
                        </div>
                        </li>
                        <li>
                        <img src="/img/slides/4.jpg"> <!-- random image -->
                        <div class="caption center-align">
                            <h3>Paseos en Kayak!</h3>
                        </div>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col s12 section-title">
                        Nuestras Actividades
                    </div>  
                </div>   
                <div class='container'>   
                    ${list()}
                </div>
                ${modal()}
            </main>`;	
}

function init() {
    $('.slider').slider({
        indicators:false,
    });    

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
        }
    );       

    $('.modal').modal()
}


//Tarjeta Prueba
/*    <div class="col s12 m6 l4">
        <div class="card">
        <div class="card-image">
            <img src="/img/image.jpg">
            <span class="card-title">Card Title</span>
        </div>
        <div class="card-content">
            <p>I am a very simple card. I am good at containing small bits of information.
            I am convenient because I require little markup to use effectively.</p>
        </div>
        </div>
    </div>   */
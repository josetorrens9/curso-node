const yo = require('yo-yo')


module.exports = function() {
    return yo `<nav>
                <div class="nav-wrapper">
                <a href='/' class='logo'>
                    <img src="/img/logo.png" alt=""/>
                </a>
                <div class='actions'>
                    <div class='social-icons'>
                        <a href=""><i class="fa fa-instagram"></i></a>
                        <a href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="user-info">
                        <a class='dropdown-button' href='' data-activates='dropdown1'>
                            <img src="/img/user.png" class='circle' alt="">
                        </a>
                        <ul id='dropdown1' class='dropdown-content'>
                            <li><a href="#modal">Crear actividad</a></li>
                            <li><a href="#!">Cerrar sesion</a></li>
                        </ul>              
                    </div>
                </div>
                </div>
            </nav>`
}
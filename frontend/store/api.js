const request = require('axios')

getList = (callback) => {
    request.get(`/api/activity`)
        .then( (res)=> {
            callback(null, res.data)
        })
        .catch( (err)=> {
            callback(err)
        })
}

getOthers = (id, callback)=> {
    request.get(`/api/activity/others/${id}`)
        .then( (res)=> {
            console.log(res)
            callback(null, res.data)
        })
        .catch( (err)=> {
            callback(err)
        })    
}

getActivity = (id, callback)=> {
    request.get(`/api/activity/${id}`)
        .then( (res)=> {
            console.log(res)
            callback(null, res.data)
        })
        .catch( (err)=> {
            callback(err)
        })
}

saveOrEditActivity = (obj, callback)=> {
    if(obj._id){
        request.put(`/api/activity/${obj._id}`, obj)
            .then( (res)=> {
                callback(null, res.data)
            })
            .catch( (err)=> {
                callback(err)
            })            
    }
    else{
        request.post(`/api/activity/`, obj)
            .then( (res)=> {
                callback(null, res.data)
            })
            .catch( (err)=> {
                callback(err)
            })     
    }
}

deleteActivity = (id, callback) =>{
    request.delete(`/api/activity/${id}`)
        .then( (res)=> {
            callback(null, res.data)
        })
        .catch( (err)=> {
            callback(err)
        })      
}





showToast = (message)=> {
    Materialize.toast(message, 4000)
}

module.exports = { getList, getOthers, getActivity, showToast, saveOrEditActivity, deleteActivity }
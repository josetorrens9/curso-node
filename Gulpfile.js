const gulp = require('gulp')
const rename = require('gulp-rename')
const babel = require('babelify')
const browserify = require('browserify')
const source = require('vinyl-source-stream')
const watchify = require('watchify')

gulp.task('styles', () =>{
	gulp
		.src('assets/css/*')
		.pipe(gulp.dest('public/css'))
})

gulp.task('images', () =>{
	gulp
		.src('assets/img/**')
		.pipe(gulp.dest('public/img'))
})

gulp.task('fonts', () =>{
  gulp
    .src('assets/fonts/*')
    .pipe(gulp.dest('public/fonts'))
})

function compile() {
  var bundle = watchify(browserify('./frontend/index.js', {debug: true}));
  bundle
    .transform(babel)
    .bundle()
    .on('error', function (err) {
      console.log(err);
      this.emit('end')
    })
    .pipe(source('index.js'))
    .pipe(rename('app.js'))
    .pipe(gulp.dest('public'));
}

gulp.task('build', function () {
  return compile();
});

gulp.task('default', ['styles', 'images', 'fonts', 'build']);


